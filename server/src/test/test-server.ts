import express from "express"
import cors from "cors"
import v1 from "../routes/v1"
import { onError } from "../handlers/JSONErrorHandler"
import Config from "../helpers/Config"
import notFoundHandler from "../handlers/NotFoundHandler"
import DB from "../model/DB"

const PORT = Config.shared.requireProduction("PORT", "8080")
const CORS_HOST = Config.shared.requireProduction(
  "CORS_HOST",
  "http://localhost:9090",
)

// Verify config
Config.shared.verify()

// Check connection to mongo db
DB.shared()
  .then(() => console.log("Connection to DB successful"))
  .catch(err => {
    console.log(
      "Unable to connect to mongo DB, check if mongod service started.",
      err,
    )
    process.exit(0)
  })

const app = express()

const start = () => {
  // Set up CORS
  const corsOptions: cors.CorsOptions = {
    allowedHeaders:    ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
    credentials:       true,
    methods:           "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
    origin:            CORS_HOST,
    preflightContinue: false,
  }
  app.use(cors(corsOptions))
  app.options("*", cors(corsOptions))

  // Set up router
  app.use("/v1", v1)

  // Handle not found route
  app.use(notFoundHandler)

  // Handle error
  app.use(onError)

  // Listen and serve
  return app.listen(PORT, () => console.log(`Test Server listening on port ${PORT}`))
}

export default {
  start,
}