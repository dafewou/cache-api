import chai, { expect, assert, should } from "chai"
import chaiHttp from "chai-http"
import Server from "../test-server"
import * as http from "http"
import { CacheObject } from "../../model/Cache"

should()
chai.use(chaiHttp)

describe("Cache", () => {
  let server: http.Server
  let cache: CacheObject

  before("All tests, start the server", async () => {
    server = await Server.start()
  })

  describe("/GET caches/:key", () => {

    it("should create new cache and return data", async () => {
      const request = await chai.request(server)
      const res = await request.get("/v1/caches/testcache1")
      cache = res.body.cache
      res.status.should.eql(201)
      expect(res.body.cache).to.be.an("object")
    })

    it("should get existing cache and return data", async () => {
      const request = await chai.request(server)
      const res = await request.get("/v1/caches/testcache1")
      assert(cache.text === res.body.cache.text, "data is cached")

      res.status.should.eql(200)
      expect(res.body.cache).to.be.an("object")
    })

    it("should create new string for new key", async () => {
      const request = await chai.request(server)
      const res = await request.get("/v1/caches/testcache2")
      assert(cache.text !== res.body.cache.text, "data is cached")

      res.status.should.eql(201)
      expect(res.body.cache).to.be.an("object")
    })

    it("should create new string for new key", async () => {
      const request = await chai.request(server)
      const res = await request.get("/v1/caches/testcache3")
      assert(cache.text !== res.body.cache.text, "data is cached")

      res.status.should.eql(201)
      expect(res.body.cache).to.be.an("object")
    })
  })

  describe("/PUT caches/:key", () => {

    it("should generate new string and update cache", async () => {
      const request = await chai.request(server)
      const res = await request.put("/v1/caches/testcache1")
      res.status.should.eql(200)
      assert(cache.text !== res.body.cache.text, "cache is updated")
      assert(cache.key === res.body.cache.key, "cache key is same")
    })
  })

  describe("/DELETE caches/:key", () => {

    it("should delete existing cache", async () => {
      const request = await chai.request(server)
      const res = await request.delete("/v1/caches/testcache1")
      res.status.should.eql(204)
    })

    it("should return not found when key not in cache", async () => {
      const request = await chai.request(server)
      const res = await request.delete("/v1/caches/testcache1")
      res.status.should.eql(404)
      should().exist(res.error)
      assert(res.body.type === "resource_not_found", "error type not correct")
    })
  })

  describe("/DELETE caches", () => {

    it("should delete all existing cache and return count", async () => {
      const request = await chai.request(server)
      const res = await request.delete("/v1/caches")
      res.status.should.eql(200)
      assert(res.body.deletedCount === 2, "deleted all caches")
    })
  })

  after("All tests, close server", async () => {
    await server.close()
  })
})
