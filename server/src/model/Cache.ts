import mongoose, { Schema, Document } from "mongoose"
import moment, { Moment } from "moment"

// Cache document Interface
export interface ICache extends Document {
  _id: string
  key: string
  text: string
  ttl: string,
  created_at: string
  last_read_at: string
}

// Cache object
export class CacheObject {
  id: string
  key: string
  text: string
  ttl: string
  createdAt: Moment
  lastReadAt: Moment

  constructor({ _id, key, text, ttl, created_at, last_read_at }: ICache) {
    this.id = _id
    this.key = key
    this.text = text
    this.ttl = ttl
    this.createdAt = moment(created_at)
    this.lastReadAt = moment(last_read_at)
  }

  redacted() {
    return {
      key:      this.key,
      text:     this.text,
      lastRead: this.lastReadAt,
    }
  }
}

// Create the Cache Schema.
const CacheSchema: Schema = new Schema({
  key:          {
    type:      String,
    index:     { unique: true },
    required:  true,
    minlength: 8,
    maxlength: 10,
  },
  text:         {
    type:     String,
    required: true,
  },
  ttl:          {
    type:     Number,
    required: true,
  },
  created_at:   {
    type:     Date,
    required: true,
  },
  last_read_at: {
    type:     Date,
    required: true,
  },
})

export default mongoose.model<ICache>("Cache", CacheSchema)
