import { Request, Response, NextFunction } from "express"
import CacheStore from "../store/CacheStore"

const onGet = async (req: Request, res: Response, next: NextFunction) => {
  const { page = 1, maxResults = 30 } = req.query

  try {
    // Fetch caches and paginationInfo
    const caches = await CacheStore.fetchAll(parseInt(page as string), parseInt(maxResults as string))

    // Filter out expired cached data
    const filteredCaches = caches.filter(cache => !CacheStore.isExpired(cache))
    const expiredCaches = caches.filter(cache => CacheStore.isExpired(cache))

    // Delete all expired cached data
    await CacheStore.deleteByKeys(expiredCaches.map(cache => cache.key))

    const paginationInfo = await CacheStore.fetchPaginationInfo(parseInt(page as string), parseInt(maxResults as string))

    // Response
    return res.status(200).json({
      caches: filteredCaches.map(cache => cache.redacted()),
      paginationInfo,
    })
  } catch (error) {
    next(error)
    return
  }
}

const onDelete = async (_req: Request, res: Response, next: NextFunction) => {
  try {
    // Removes all keys from the cache
    const { deletedCount } = await CacheStore.deleteAll()

    // Response
    return res.status(200).json({
      deletedCount,
    })
  } catch (error) {
    next(error)
    return
  }
}

const CachesHandler = {
  onGet,
  onDelete,
}

export default CachesHandler
