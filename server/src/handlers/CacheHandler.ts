import { Request, Response, NextFunction } from "express"
import { ResourceNotFoundError } from "../errors"
import CacheStore from "../store/CacheStore"
import Logger from "../helpers/Logger"
import moment from "moment"
import Config from "../helpers/Config"

const MAX_CACHED_ITEM = parseInt(Config.shared.requireProduction("MAX_CACHED_ITEM", "10"))

const onGet = async (req: Request, res: Response, next: NextFunction) => {
  const { key } = req.params

  try {
    const currentDate = moment().format("YYYY-MM-DD hh:mm:ss")
    // Fetch cache by key
    const cache = await CacheStore.fetchByKey(key)

    if (cache == null) {
      Logger.debug(`${currentDate} - Cache miss`)

      // Check if max entries is reached and overwrite old entry
      const totalCount = await CacheStore.getTotalCount()
      if (totalCount === MAX_CACHED_ITEM) {
        // Fetch oldest cache data by filtering by last_read_at
        const oldestCache = await CacheStore.fetchOldest()

        // Delete oldest entry
        await CacheStore.delete(oldestCache?.key as string)
      }

      // Create new random string in cache
      const newCache = await CacheStore.create(key)
      // Response
      return res.status(201).json({
        cache: newCache.redacted(),
      })
    }

    // Delete cache if data expired
    if (CacheStore.isExpired(cache)) {
      Logger.debug(`${currentDate} - Cache expired`)
      await CacheStore.delete(key)
      next(new ResourceNotFoundError("Cache data expired"))
      return
    }

    Logger.debug(`${currentDate} - Cache hit`)
    const updateHitCache = await CacheStore.updateLastRead(key)

    // Response
    return res.status(200).json({
      cache: updateHitCache.redacted(),
    })
  } catch (error) {
    next(error)
    return
  }
}

const onDelete = async (req: Request, res: Response, next: NextFunction) => {
  const { key } = req.params

  try {
    // Fetch cache by key
    const cache = await CacheStore.fetchByKey(key)
    if (cache == null) {
      next(new ResourceNotFoundError("Cache not found"))
      return
    }

    // delete cache
    await CacheStore.delete(key)

    // Response
    return res.status(204).end()
  } catch (error) {
    next(error)
    return
  }
}

const onPut = async (req: Request, res: Response, next: NextFunction) => {
  const { key } = req.params
  const currentDate = moment().format("YYYY-MM-DD hh:mm:ss")

  try {
    // Fetch cache by key
    const cache = await CacheStore.fetchByKey(key)
    if (cache == null) {
      next(new ResourceNotFoundError("Cache not found"))
      return
    }

    // Check for expiry cache
    if (CacheStore.isExpired(cache)) {
      Logger.debug(`${currentDate} - Cache expired`)
      await CacheStore.delete(key)
      next(new ResourceNotFoundError("Cache data expired"))
      return
    }

    // Update cache data
    const updatedCache = await CacheStore.update(key)

    // Response
    return res.status(200).json({
      cache: updatedCache.redacted(),
    })
  } catch (error) {
    next(error)
    return
  }
}

const CacheHandler = {
  onGet,
  onDelete,
  onPut,
}
export default CacheHandler
