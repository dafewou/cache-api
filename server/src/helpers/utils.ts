import randToken from "rand-token"

export const randomString = () => {
  return randToken.generate(15, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")
}
