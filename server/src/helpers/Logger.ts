import Config from "./Config"
import { createLogger, format, Logger, transports } from "winston";

const LOGGING_LEVEL = Config.shared.requireProduction("LOGGING_LEVEL", "info")

const Logger: Logger = createLogger({
  format: format.combine(
    format.splat(),
    format.simple(),
  ),
  level: LOGGING_LEVEL,
  transports: [
    new transports.Console(),
  ],
});

export default Logger;
