import Cache, { CacheObject, ICache } from "../model/Cache"
import PaginationInfo from "../model/PaginationInfo"
import { DatabaseError } from "../errors"
import { randomString } from "../helpers/utils"
import Config from "../helpers/Config"
import moment from "moment"

const CACHE_ITEM_TTL = Config.shared.requireProduction("CACHE_ITEM_TTL", "5")

export default class CacheStore {

  static getTotalCount = async (): Promise<number> => {
    try {
      return Cache.countDocuments()
    } catch (err) {
      throw new DatabaseError("Failed to fetch total count", err)
    }
  }

  static isExpired = (cache: CacheObject): boolean => {
    try {
      return cache.lastReadAt.add(cache.ttl, "m").isBefore(moment())
    } catch (err) {
      throw new DatabaseError("Failed to check expiry cache", err)
    }
  }

  static create = async (key: string): Promise<CacheObject> => {
    try {
      // Get ttl and random text
      const text = randomString()
      const ttl = parseInt(CACHE_ITEM_TTL)
      const currentDate = moment().format("YYYY-MM-DD hh:mm:ss")

      // create new cache model
      const cache = new Cache({ key, text, ttl, created_at: currentDate, last_read_at: currentDate })

      // save and return cache data
      await cache.save()
      return new CacheObject(cache)
    } catch (err) {
      throw new DatabaseError("Failed to create cache data", err)
    }
  }

  static update = async (key: string): Promise<CacheObject> => {
    try {
      // Get ttl and date
      const text = randomString()
      const currentDate = moment().format("YYYY-MM-DD hh:mm:ss")

      // find and update cache document
      await Cache.findOneAndUpdate({ key }, { text, last_read_at: currentDate })

      const updatedCache = await Cache.findOne({ key })

      // return updated cache
      return new CacheObject(updatedCache as ICache)
    } catch (err) {
      throw new DatabaseError("Failed to update cache", err)
    }
  }

  static updateLastRead = async (key: string): Promise<CacheObject> => {
    try {
      const currentDate = moment().format("YYYY-MM-DD hh:mm:ss")

      // find and update cache document
      await Cache.findOneAndUpdate({ key }, { last_read_at: currentDate })

      const updatedCache = await Cache.findOne({ key })

      // return updated cache
      return new CacheObject(updatedCache as ICache)
    } catch (err) {
      throw new DatabaseError("Failed to update cache", err)
    }
  }

  static delete = async (key: string): Promise<CacheObject> => {
    try {
      // find and delete cache
      const deletedCache = await Cache.findOneAndDelete({ key })

      // return deleted cache
      return new CacheObject(deletedCache as ICache)
    } catch (err) {
      throw new DatabaseError("Failed to delete cache", err)
    }
  }

  static fetchByKey = async (key: string): Promise<CacheObject | null> => {
    try {
      // Fetch cache
      const cache = await Cache.findOne({ key })

      // Return cache
      return new CacheObject(cache as ICache)
    } catch (err) {
      // Return null on no match
      return null
    }
  }

  static fetchAll = async (page: number = 1, maxResults: number = 30): Promise<CacheObject[]> => {
    try {
      // fetch all caches
      const caches = await Cache.find({}, null, { skip: (maxResults * (page - 1)), limit: maxResults })

      // return list of user caches
      return caches.map(cache => new CacheObject(cache))
    } catch (err) {
      throw new DatabaseError("Failed to fetch all caches", err)
    }
  }

  static fetchOldest = async (): Promise<CacheObject | null> => {
    try {
      // fetch oldest cache
      const [cache] = await Cache.find({}, null, { limit: 1 }).sort({ last_read_at: "asc" })

      // Return cache
      return new CacheObject(cache as ICache)
    } catch (err) {
      throw new DatabaseError("Failed to fetch oldest cache", err)
    }
  }

  static deleteAll = async () => {
    try {
      // delete all caches
      return Cache.deleteMany({})
    } catch (err) {
      throw new DatabaseError("Failed to delete all caches", err)
    }
  }

  static deleteByKeys = async (keys: string[]) => {
    try {
      // delete all caches
      return Cache.deleteMany({ key: { $in: keys } })
    } catch (err) {
      throw new DatabaseError("Failed to delete by keys", err)
    }
  }

  static fetchPaginationInfo = async (page: number = 1, maxResults: number = 30): Promise<PaginationInfo> => {
    try {
      // fetch all cache doc
      const totalCaches = await Cache.countDocuments()

      // return pagination info
      return new PaginationInfo(totalCaches, page, maxResults)
    } catch (err) {
      throw new DatabaseError("Failed to fetch cache pagination", err)
    }
  }
}
