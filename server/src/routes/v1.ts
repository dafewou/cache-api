import express from "express"
import bodyParser from "body-parser"
import CachesHandler from "../handlers/CachesHandler"
import CacheHandler from "../handlers/CacheHandler"

const router = express.Router()

// Middleware
router.use(bodyParser.json())

/*
 * Routes
 */
// Caches
router.route("/caches")
  .get(CachesHandler.onGet)
  .delete(CachesHandler.onDelete)

router.route("/caches/:key")
  .get(CacheHandler.onGet)
  .put(CacheHandler.onPut)
  .delete(CacheHandler.onDelete)

export default router